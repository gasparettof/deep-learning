import sys
import torch
from torchvision import models, transforms
from PIL import Image, ImageDraw
import matplotlib.pyplot as plt
from torchvision.models import ResNet50_Weights

# Carica il modello preaddestrato ResNet-50
model = models.resnet50(weights=ResNet50_Weights.DEFAULT)
model.eval()

# Carica le etichette delle classi di ImageNet
with open('dataset_imagenet/imagenet_classes.txt') as f:
    labels = [line.strip() for line in f.readlines()]

# Carica l'immagine dal percorso fornito come argomento
image_path = sys.argv[1]
input_image = Image.open(image_path)

# Preprocessa l'immagine
preprocess = transforms.Compose([
    transforms.Resize(256),
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
])

# Ottenere la classe target da linea di comando
target_class = sys.argv[2]

min_squares = []
max_score = 0
square_sizes = [100, 50, 150]

for square_size in square_sizes:
    for x in range(0, input_image.size[0], square_size):
        for y in range(0, input_image.size[1], square_size):
            square = input_image.crop((x, y, x + square_size, y + square_size))
            input_tensor = preprocess(square).unsqueeze(0)

            # Effettua la predizione sulla sezione del quadrato
            output = model(input_tensor)
            _, predicted_idx = torch.max(output, 1)
            predicted_class = labels[predicted_idx]

            # Ottieni il punteggio di match
            score = torch.nn.functional.softmax(output, dim=1)[0][predicted_idx].item()

            # Stampa informazioni sulla classe predetta
            print(f"Predizione per il quadrato ({x},{y}) - Classe: {predicted_class}, Punteggio: {score}")

            # Memorizza i dettagli del quadrato con il punteggio più alto
            if predicted_class == target_class and score > max_score:
                min_squares = [(square, x, y, square_size)]
                max_score = score

# Disegna il bordo rosso attorno al quadrato con il punteggio più alto
if min_squares:
    square, x, y, s_size = min_squares[0]
    draw = ImageDraw.Draw(input_image)
    draw.rectangle((x, y, x + s_size, y + s_size), outline='red', width=3)
    plt.imshow(input_image)
    plt.axis('off')
    plt.show()
else:
    print("Nessun quadrato con l'oggetto della classe target trovato.")
