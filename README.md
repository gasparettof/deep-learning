# Handwritten Digit Recognition

This project implements a simple neural network using PyTorch for recognizing handwritten digits from the MNIST dataset. The model is trained to classify digits ranging from 0 to 9.

## Features

- Load and preprocess the MNIST dataset.
- Define and train a neural network.
- Evaluate the model on a test dataset.
- Save the trained model for future use.

## Getting Started

### Prerequisites

Make sure you have the following libraries installed:

- `torch`
- `torchvision`

You can install them using pip:

```sh
pip install torch torchvision
```

## MNIST Dataset

The MNIST dataset is a large database of handwritten digits that is commonly used for training various image processing systems. It contains the following:

- **Training Set**: 60,000 images of handwritten digits.
- **Test Set**: 10,000 images of handwritten digits.
- **Image Size**: 28x28 pixels.
- **Color**: Grayscale.
- **Classes**: 10 classes (digits 0-9).

Example images from the MNIST dataset:

![MNIST Digits](https://upload.wikimedia.org/wikipedia/commons/2/27/MnistExamples.png)

### Characteristics of the MNIST Dataset

1. **Digit Classes**: The dataset includes digits from 0 to 9.
2. **Image Format**: Each image is a 28x28 pixel grayscale image.
3. **Training and Test Sets**:
   - The training set contains 60,000 images.
   - The test set contains 10,000 images.
4. **Normalization**: Images are often normalized to have pixel values between 0 and 1 before being fed into the neural network.

The dataset is well-suited for benchmarking machine learning algorithms as it is simple enough to allow for quick prototyping and testing, yet it provides a robust challenge for image classification tasks.
