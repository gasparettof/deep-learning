import os
import random
from PIL import Image, ImageDraw

# Directory in cui salvare le immagini
output_dir = 'shapes_dataset_new'
os.makedirs(output_dir, exist_ok=True)

# Sottocartelle per ogni forma
shapes = ['circle', 'square', 'triangle']
for shape in shapes:
    os.makedirs(os.path.join(output_dir, shape), exist_ok=True)

# Dimensioni delle immagini
image_size = 128

# Numero di immagini per ogni forma
num_images_per_shape = 10000 // len(shapes)  # Dividiamo equamente tra le forme

# Funzione per generare un'immagine contenente un cerchio
def create_circle_image(filename):
    image = Image.new('RGB', (image_size, image_size), 'white')
    draw = ImageDraw.Draw(image)
    radius = random.randint(10, image_size // 2 - 10)
    x = random.randint(radius, image_size - radius)
    y = random.randint(radius, image_size - radius)
    draw.ellipse((x - radius, y - radius, x + radius, y + radius), fill='black')
    image.save(filename)

# Funzione per generare un'immagine contenente un quadrato
def create_square_image(filename):
    image = Image.new('RGB', (image_size, image_size), 'white')
    draw = ImageDraw.Draw(image)
    side = random.randint(20, image_size - 20)
    x = random.randint(0, image_size - side)
    y = random.randint(0, image_size - side)
    draw.rectangle((x, y, x + side, y + side), fill='black')
    image.save(filename)

# Funzione per generare un'immagine contenente un triangolo
def create_triangle_image(filename):
    image = Image.new('RGB', (image_size, image_size), 'white')
    draw = ImageDraw.Draw(image)
    p1 = (random.randint(10, image_size - 10), random.randint(10, image_size - 10))
    p2 = (random.randint(10, image_size - 10), random.randint(10, image_size - 10))
    p3 = (random.randint(10, image_size - 10), random.randint(10, image_size - 10))
    draw.polygon([p1, p2, p3], fill='black')
    image.save(filename)

# Generazione delle immagini
for shape in shapes:
    for i in range(num_images_per_shape):
        filename = os.path.join(output_dir, shape, f'{shape}_{i}.png')
        if shape == 'circle':
            create_circle_image(filename)
        elif shape == 'square':
            create_square_image(filename)
        elif shape == 'triangle':
            create_triangle_image(filename)

print("Dataset creato con successo")
